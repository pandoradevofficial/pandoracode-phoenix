<?php

class Web
{

    public static function url($url, $controller, $id = 0)
    {

        $baseUrl         = route_params();

        $removeLastSlash = explode('/', route_params());

        if ($removeLastSlash[count($removeLastSlash) - 1] == "") {

            array_splice($removeLastSlash, count($removeLastSlash) - 1);

            $baseUrl = $removeLastSlash;

            $baseUrl = (empty($baseUrl[0])) ? $removeLastSlash : $baseUrl[0];
        }

        if (strHas($url, "$")) {

            $url       = explode("/", $url);

            $nameField = explode('$', $url[count($url) - 1]);

            $nameField = $nameField[1];

            array_splice($url, count($url) - 1);

            $baseUrl   = explode('/', route_params());

            $url[]     = $baseUrl[count($baseUrl) - 1];

            $value     = $url[count($url) - 1];
        }

        if ($url == $baseUrl) {

            if (empty($id)) {

                $id = $id;
            } else {

                $id = $_GET['id'];
            }

            $controller  = explode("@", $controller);

            // check($controller);

            if(is_file(dir_project() . "controller/" . $controller[0] . ".php")){

                include dir_project() . "controller/" . $controller[0] . ".php";
    
                if (!function_exists($controller[1])) {
    
                    $file     = controller[0];
                    $function = controller[1];
    
                    @include dir_project() . "app/resource/views/errors/error-controller.php";
                } else {
    
                    if (!empty($value)) {
                        $_REQUEST[$nameField]   = $value;
                    }
    
    
                    @$controller[1]($object = json_decode(json_encode($_REQUEST)), $id);
                }
            }else{
                throwMyCustomException(
                    "Tidak ditemukan controller dengan nama $controller[0]",
                    dir_project() . "controller/" . $controller[0] . ".php",
                    0
                );
            }
        }
    }


    public static function resource($url, $controller)
    {

        // check($url);

        $praresource = Web::url($url, $controller . '@index');
        $praresource = Web::url('create-' . $url, $controller . '@create');
        $praresource = Web::url('store-' . $url, $controller . '@store');
        $praresource = Web::url('edit-' . $url . '/$id', $controller . '@editData');
        $praresource = Web::url('update-' . $url . '/$id', $controller . '@updateData');

        return $praresource;
    }

    public function view($url, $view)
    {

        $baseUrl         = route_params();

        $removeLastSlash = explode('/', route_params());

        if ($removeLastSlash[count($removeLastSlash) - 1] == "") {

            array_splice($removeLastSlash, count($removeLastSlash) - 1);

            $baseUrl = $removeLastSlash;

            @$baseUrl = $baseUrl[0];
        }

        if ($url == $baseUrl) {

            if (strHas($view, "backend")) {

                global $backendHeader;
                global $backendFooter;

                if (@file_exists(dir_project() . 'resource/views/' . $view . '.php')) {

                    if (!urlHas('backend/auth')) {

                        $no = 1;

                        @include layouts($backendHeader);
                        @include dir_view($view);
                        @include layouts($backendFooter);
                    } else {

                        @include dir_view(route_params());
                    }
                } else {

                    notFound();
                }

                die();
            }

            if (strHas($view, "frontend")) {
                global $frontendHeader;
                global $frontendFooter;

                if (@file_exists(dir_project() . 'resource/views/' . $view . '.php')) {

                    if (!urlHas('backend/auth')) {

                        $no = 1;

                        @include layouts($frontendHeader);
                        @include dir_view($view);
                        @include layouts($frontendFooter);
                    } else {

                        @include dir_view(route_params());
                    }
                } else {

                    notFound();
                }

                die();
            }
        }
    }
}
