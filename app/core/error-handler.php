<?php

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        return false;
    }

    $errmessage = htmlspecialchars($errstr);

    switch ($errno) {
    case E_USER_ERROR:
        
        include dir_project()."app/resource/views/errors/error-handling.php";

        exit(1);

    case E_USER_WARNING:

        include dir_project()."app/resource/views/errors/error-handling.php";

        break;

    case E_USER_NOTICE:

        include dir_project()."app/resource/views/errors/error-handling.php";

        break;

    default:

        include dir_project()."app/resource/views/errors/error-handling.php";

        break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

$old_error_handler = set_error_handler("myErrorHandler");

class MyCustomException extends ErrorException  { }

function throwMyCustomException($message,$file,$line) {
    throw new MyCustomException($message,999,1,$file,$line,);
}

/*
    LIST CUSTOM ERROR CODE

    999 = controller error
*/ 




//  patern
// error_function(error_level,error_message,error_file,error_line,error_context)
