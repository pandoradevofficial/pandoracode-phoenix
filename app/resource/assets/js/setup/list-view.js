$('.paket_crud').on('change', function() {

    if ($(this).is(':checked')) {

        $('.create-file').hide();
        $('.file-name').removeAttr('required');

    }else{

        $('.create-file').show();
        $('.file-name').attr('required','');

    }

})

$('.dynamic_url').on('change', function() {

    if ($(this).is(':checked')) {

        $('.custom_resource').show();
        $('.simple_route').show();
        $('.dynamic_desc').html(", untuk costum resource jika dikosongi akan otomatis mengikuti nama table.");

    }else{

        $('.custom_resource').hide();
        $('.simple_route').hide();
        $('.dynamic_desc').html("");

    }

})