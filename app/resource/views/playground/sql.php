<div class="bg-dark">
    <div class="container  m-b-30">
        <div class="row">
            <div class="col-12 text-white p-t-40 p-b-90">

                <h4 class="">
                    Playground SQL (Structure Query Languange) <img src="<?= asset_setup('setup/controller.png') ?>" style="max-width: 25px;" alt="">
                </h4>
                <p class="opacity-75 ">
                    Di halaman ini anda bisa bermain dengan kode kode SQL
                </p>


            </div>
        </div>
    </div>
</div>

<div class="container  pull-up">

    <div class="row">


        <div class="col-lg-12 m-b-30">
            <div class="card m-b-30">
                <!-- <div class="card-header">
                    <h5 class="card-title m-b-0">Structure Query Languange</h5>

                    <div class="card-controls">

                        <a href="#" class="js-card-fullscreen icon"></a>

                    </div>
                </div> -->
                <div class="card-body">

                    <form action="<?= controllerSetup('SetupController@store') ?>" method="POST" enctype="multipart/form-data">
                        <div class="card-body">

                            <div class="form-group row">
                                <!-- <label class="col-sm-2 col-form-label">Nama Controller </label> -->
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <textarea class="form-control text-sql" name="controller" type="text" required placeholder="Amazing SQL" style="font-size: large;"></textarea>
                                            <small id="emailHelp" class="form-text text-muted">Tulis SQL yang mau kamu coba</small>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-10">
                                    <small class="form-text text-info mb-2">PRIMARY COMMAND</small>
                                    <button type="button" class="btn btn-sm btn-primary btn-insert">INSERT</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-select">SELECT</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-update">UPDATE</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-delete">DELETE</button>
                                    <div class="w-100"></div>

                                    <small class="form-text text-info mb-2">TOPPING COMMAND</small>
                                    <button type="button" class="btn btn-sm btn-primary btn-where">WHERE</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-left-join">LEFT JOIN</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-join">JOIN</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-right-join">RIGHT JOIN</button>
                                    <button type="button" class="btn btn-sm btn-primary btn-order-by">ORDER BY</button>
                                    <div class="w-100"></div>

                                </div>
                                <div class="col-md-2 text-right">
                                    <button type="submit" class="btn btn-sm btn-primary">RUN</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

            <div class="card m-b-30">
                <div class="card-header">
                    <h5 class="card-title m-b-0">Hasil Query</h5>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
<?php content('script') ?>
<script>
    var text_sql = $(".text-sql");
    $(document).ready(function(){

        $(".btn-insert").on("click", function(){
            text_sql.val("INSERT INTO `table_name` VALUES(`data_1`)");
        });
        
        $(".btn-select").on("click", function(){
            text_sql.val("SELECT * FROM `table_name`");
        });

        $(".btn-update").on("click", function(){
            text_sql.val("UPDATE `table_name` SET column_name = `data_1` WHERE column_name = ???");
        });

        $(".btn-delete").on("click", function(){
            text_sql.val("DELETE FROM `table_name` WHERE column_name = ???");
        });

        // TOPPING COMMAND

        $(".btn-where").on("click", function(){
            text_sql.val(text_sql.val() + " WHERE column_name = ???");
        });

        $(".btn-left-join").on("click", function(){
            text_sql.val(text_sql.val() + " LEFT JOIN table_join ON table.column_name = table_join.column_name");
        });

        $(".btn-join").on("click", function(){
            text_sql.val(text_sql.val() + "");
        });
        
        $(".btn-right-join").on("click", function(){
            text_sql.val(text_sql.val() + "");
        });

        $(".btn-order-by").on("click", function(){
            text_sql.val(text_sql.val() + "");
        });
        $(".btn-delete").on("click", function(){
            text_sql.val("DELETE FROM `table_name` WHERE column_name = ???");
        });

    });
</script>
<?php endContent('script') ?>