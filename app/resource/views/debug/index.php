<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Pandoracode | Debug</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= asset_setup('css/style-debug.css') ?>" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/prism-tomorrow.min.css') ?>">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/prism-line-highlight.css') ?>">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/line-number.min.css') ?>">
</head>

<body >
    <header data-plugin-header="keep-markup"></header>
    <div class="d-flex" id="wrapper">
        <!-- Sidebar-->
        <div class="border-end" style="background-color: #dfe4ea;" id="sidebar-wrapper">
            <!-- <div class="sidebar-heading border-bottom bg-light" style="font-family: 'Rubik', sans-serif;font-size:16px">PandoraDebug</div> -->
            
            <?php

                
                if (empty($datas) || $datas == '' || $datas == null || $datas == 'null') {
                    
                    $datas = mysqli_error($host);

                    $readFile = false;

                    if (!empty($controller)) {
                        
                        $controller = explode('\\', debug_backtrace()[2]['file']);

                        
                        if (strHas($controller, "controller")) {
                            $controller = $controller[array_key_last($controller)];
                        }else{
                            $controller = "-";
                        }
                        
                    }

                }else{
                    $readFile = true;
                    
                    foreach (debug_backtrace()[2] as $key => $value) {
                        $controller = $value[0]->controllerName;
                    }
                    
                    
                    if (!$controller) {
                        
                        $controller = explode('\\', debug_backtrace()[1]['file']);
                        // var_pre(debug_backtrace()[1]['file']);
                        
                        if (strHas($controller, "controller")) {
                            $controller = $controller[array_key_last($controller)];
                        }else{
                            $controller = "-";
                        }
                        
                    }
                    
                }

                if (debug_backtrace()[2]['function'] == "include") {
                        
                    $function = "-";

                }else{

                    $function = debug_backtrace()[2]['function'];

                }
            ?>

            <div class="list-group list-group-flush" style="margin-top:-10px">
                <a class="list-group-item list-group-item-action list-group-item-light p-3">
                    Request
                    <button type="button" class="all-request" style="max-height: 100px;" data-toggle="modal" data-target="#allrequest">Detail Request</button>
                </a>
                <pre class="res"><code class="language-css">File       : <?= $controller ?> <br>
Function   : <?= $function ?></code></pre>

                <a style="margin-top: -5px; background-color:#ecf0f1;color:#3d3d3d" class="list-group-item-new-light list-group-item-action p-3">
                    Response
                    <button type="button" class="all-response" style="max-height: 100px;" data-toggle="modal" data-target="#debugBacktrace">Debug Backtrace</button>
                </a>
                <pre class="res"><code class="language-php"><?= pre($datas) ?></code></pre>
            </div>


            <!-- Modal all request-->
            <div class="modal fade" id="allrequest" tabindex="-1" role="dialog" aria-labelledby="allrequestLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-body" style="background-color: #120f12;">
                            <span style="color:white"><b>Detail Request</b></span>
                            <img src="<?= asset_setup('img/close.png') ?>" class="closed" alt="" data-dismiss="modal">
                            <pre style="background-color: #120f12;" class="line-numbers"><code class="language-php"><?php pre(debug_backtrace()[2]['args']) ?></code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Debug backtrace-->
            <div class="modal fade" id="debugBacktrace" tabindex="-1" role="dialog" aria-labelledby="debugBacktraceLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-body" style="background-color: #120f12;">
                            <span style="color:white"><b>Debug Backtrace</b></span>
                            <img src="<?= asset_setup('img/close.png') ?>" class="closed" alt="" data-dismiss="modal">
                            <pre style="background-color: #120f12;" class="line-numbers"><code class="language-php"><?php pre(debug_backtrace()) ?></code></pre>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- Page content wrapper-->
        <div id="page-content-wrapper">

            <!-- Page content-->
            <div class="container-fluid mt-2">
                <div class="row">

                    <div class="col-md-12">
                        <div style="background-color: white;padding:5px;color:#4040c8;font-weight: 600;">Data Response</div>
                        <?php 
                            $search = null;
                            if (empty($anchor)) {
                                $search = $datas;
                                ?>
                            <pre style="overflow-y: scroll; max-height:500px;background-color: #120f12;margin-top:0" class="line-numbers"><code class="language-php"><?= pre($datas) ?></code></pre>
                        <?php 
                            } else { 
                                $search = $anchor;
                        ?>
                            <pre style="overflow-y: scroll; max-height:500px;background-color: #120f12;margin-top:0" class="line-numbers"><code class="language-php"><?= pre($anchor) ?></code></pre>
                        <?php } ?>
                    </div>
                    
                    <div class="col-md-12">

                        
                        <?php if($readFile){ ?>
                            <div style="background-color: white;padding:5px;color:#4040c8;font-weight: 600;"><?= debug_backtrace()[1]['file'] ?> (Line : <?= debug_backtrace()[1]['line'] ?>)</div>
                            <pre style="white-space: pre-wrap;background-color: #120f12;max-height:450px;margin-top:0" data-line="<?= debug_backtrace()[1]['line'] ?>" data-src="<?= asset_setup('js/debug/prism-line-highlight.js') ?>" class="line-numbers"><code class=" language-php"><?php str_replace("1","",highlight_string(file_get_contents(debug_backtrace()[1]['file']))) ?></code></pre>
                        <?php }else{ ?>
                            <div style="background-color: white;padding:5px;color:#4040c8;font-weight: 600;"><?= debug_backtrace()[2]['file'] ?> (Line : <?= debug_backtrace()[2]['line'] ?>)</div>
                            <pre style="background-color: #120f12;max-height:450px;margin-top:0" data-line="<?= debug_backtrace()[2]['line'] ?>" data-src="<?= asset_setup('js/debug/prism-line-highlight.js') ?>" class="line-numbers"><code class="language-php"><?php str_replace("1","",highlight_string(file_get_contents(debug_backtrace()[2]['file']))) ?></code></pre>
                        <?php } ?>

                    </div>

                    <div class="col-md-12">
                        <div style="background-color: white;padding:5px;color:#4040c8;font-weight: 600;">Solution</div>
                        <iframe src="https://www.google.com/search?igu=1&q=<?= $search ?>" class="w-100" style="height:100vh"></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Core theme JS-->
    
    <script type="application/javascript" src="<?= asset_setup('js/debug/jquery.slim.min.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/scripts.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/prism-core.min.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/precode.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/prism-line-numbers.min.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/bootstrap.bundle.min.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/prism-line-highlight.js') ?>"></script>
    <script type="application/javascript" src="<?= asset_setup('js/debug/prism-autoloader.min.js') ?>"></script>

</body>

</html>