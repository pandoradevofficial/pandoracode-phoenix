<?php
$errLine    = (empty($errline))  ? $error->getLine()      : $errline;
$errFile    = (empty($errfile))  ? $error->getFile()      : $errfile;
$errMessage = (empty($errmessage)) ? $error->getMessage() : $errmessage;
$errCode = (empty($errcode)) ? "" : $errcode;
?>
<!DOCTYPE html>
<html lang="ind">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Pandoracode | Debug</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= asset_setup('css/style-debug.css') ?>" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/prism-tomorrow.min.css') ?>">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/prism-line-highlight.css') ?>">
    <link rel="stylesheet" href="<?= asset_setup('css/debug/line-number.min.css') ?>">
</head>

<style>
    body {
        background-color: #ecf0f1;
    }

    .header {

        background: url('<?= asset_setup('img/error/bg.png') ?>');
        background-size: contain;
        background-color: #303952;
        background-repeat: no-repeat;
        background-position: center right;
        padding: 40px;

    }

    h3 {
        color: white;
        font-size: 40px;
    }

    .nav {
        margin-top: 40px;
        border: none;
        font-weight: 600;
        margin-left: -13px;
    }

    .nav-link:hover {
        border: none;
    }

    .nav-tabs .nav-link {
        border: none;
    }

    .nav-tabs .nav-link.active {
        background-color: transparent;
        border: none;
        color: white;
    }

    .active {
        background-color: transparent;
        border: none;
    }

    .nav-tabs .nav-link.active:hover {
        background-color: transparent;
        border: none;
        color: #BADC58;
    }

    a {
        color: white !important;
    }

    a:hover {
        color: #BADC58 !important;
    }

    .msg-header {
        background-color: #BADC58;
        margin-top: -23px;
        padding: 6px;
    }

    .msg-text {
        font-size: 23px;
        margin-left: 20px;
        font-weight: 600;
    }

    .file-location {
        margin-left: 20px;
        font-size: 14px;
    }

    .card {
        border: none;
    }

    .card-header {
        border: none;
        border-radius: 0 !important;
    }
</style>

<body>

    <div style="overflow-y:auto;position: fixed; top:0px; right:0px; bottom:0px; left:0px; width:100%; height:100%;z-index: 1200;">

        <div class="header">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h3>Hmmm... <br> Something Went Wrong.</h3>

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Errors</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Solution</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Debug Backtrace</a>
                            </div>
                        </nav>

                    </div>
                </div>

            </div>

        </div>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-header msg-header">

                                    <h1 class="msg-text">Error : lorem</h1>

                                </div>

                                <div class="card-body">

                                    <div class="media">
                                        <div class="media-left">
                                            <img src="<?= asset_setup('img/error/light-bulb.png') ?>" style="max-width: 40px;" alt="">
                                        </div>
                                        <div class="media-right">
                                            <p><?= $errMessage.$errLine.
$errFile ?></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-header msg-header">

                                    <h1 class="msg-text">Error : lorem</h1>
                                    <span class="file-location">controller/BukuController.php</span>

                                </div>

                                <div class="card-body">

                                    <div class="media">
                                        <div class="media-left">
                                            <img src="light-bulb.png" style="max-width: 40px;" alt="">
                                        </div>
                                        <div class="media-right">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias sequi
                                                repudiandae quidem voluptas vitae laborum, tempore quisquam blanditiis sint
                                                eum? Explicabo inventore repudiandae dolores laudantium natus voluptas harum
                                                nemo repellendus?</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-header msg-header">

                                    <h1 class="msg-text">Error : lorem</h1>
                                    <span class="file-location">controller/BukuController.php</span>

                                </div>

                                <div class="card-body">

                                    <div class="media">
                                        <div class="media-left">
                                            <img src="light-bulb.png" style="max-width: 40px;" alt="">
                                        </div>
                                        <div class="media-right">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias sequi
                                                repudiandae quidem voluptas vitae laborum, tempore quisquam blanditiis sint
                                                eum? Explicabo inventore repudiandae dolores laudantium natus voluptas harum
                                                nemo repellendus?</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>



<!-- Core theme JS-->

<script type="application/javascript" src="<?= asset_setup('vendor/jquery/jquery.min.js') ?>"></script>
<!-- <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> -->

<!-- <script type="application/javascript" src="<?= asset_setup('js/scripts.js') ?>"></script> -->
<script type="application/javascript" src="<?= asset_setup('js/debug/prism-core.min.js') ?>"></script>
<script type="application/javascript" src="<?= asset_setup('js/debug/precode.js') ?>"></script>
<script type="application/javascript" src="<?= asset_setup('js/debug/prism-line-numbers.min.js') ?>"></script>
<script type="application/javascript" src="<?= asset_setup('js/debug/bootstrap.bundle.min.js') ?>"></script>
<script type="application/javascript" src="<?= asset_setup('js/debug/prism-line-highlight.js') ?>"></script>
<script type="application/javascript" src="<?= asset_setup('js/debug/prism-autoloader.min.js') ?>"></script>
<script>
    $(document).ready(function() {

        $(".nav-link").click(function() {
            $('.block-code').animate({
                scrollTop: $("#bottom-div").offset().top
            }, 2000);
        });

        $(".doc-code").animate({
            scrollTop: $(".line-highlight").offset().top - 100
        }, 2000);
    })
</script>

</html>