<!-- Include plugins -->
<script src="<?= asset('plugins/sweetalert2@11') ?>"></script>

<!-- Make logic for alert with session -->
<script type="text/javascript">
    <?php

    if (isset($_SESSION["title_alert"])) {

        $title      = $_SESSION["title_alert"];
        $alert      = $_SESSION["message_alert"];
        $type_alert = $_SESSION["type_alert"];

        ?>

            Swal.fire(
            '<?= $title ?>',
            '<?= $alert ?>',
            '<?= $type_alert ?>'
            );


    <?php

        }
        unset($_SESSION["title_alert"]);
        unset($_SESSION["message_alert"]);
        unset($_SESSION["type_alert"]);
        
    ?>
</script>