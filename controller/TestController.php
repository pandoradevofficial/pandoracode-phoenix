<?php

    function index()
    {

        // check('aa');
        // $data = query()->table('buku')->get();

        view('backend/buku/data');

    }

    function create()
    {

        view('backend/buku/form');

    }

    function store($request)
    {

        query()->insert('buku',[

            $request->name_inputan1,
            $request->name_inputan2

        ])->view('buku','Berhasil!');

    }

    function EditData($request)
    {

        $data = query()->table('buku')->where('id',$request->id)->single();

        view('backend/buku/form-edit', compact('data'));

    }

    function UpdateData($request)
    {

        query()->update('buku',[

            'kolom_table1' => $request->name_input1,
            'kolom_table2' => $request->name_input2

        ], $request->id)->view('buku','Berhasil!');

    }

    function HapusData($request)
    {

        $id = $request->id_delete;

        for ($i=0; $i < count($request->id_delete) ; $i++) { 
            
            query()->table('buku')->where('id', $id[$i])->delete();
            
        }

        alert('Berhasil !','Data berhasil dihapus');
        view('buku');

    }

   /*
    |--------------------------------------------------------------------------
    | PandoraCode Phoenix
    |--------------------------------------------------------------------------
    |
    | Nama File   : TestController
    | Dibuat pada : 04 Feb 2022 14:17
    | 
    */