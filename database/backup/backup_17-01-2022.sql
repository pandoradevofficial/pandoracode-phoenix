SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

-- DATABASE NAME pandoracode_development

-- CREATE TABLE aku
CREATE TABLE `aku` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE aku
INSERT INTO 'aku' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE asa
CREATE TABLE `asa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE asa
INSERT INTO 'asa' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE buku
CREATE TABLE `buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE buku
INSERT INTO 'buku' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE coba
CREATE TABLE `coba` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE coba
INSERT INTO 'coba' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE dssds
CREATE TABLE `dssds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE dssds
INSERT INTO 'dssds' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE po
CREATE TABLE `po` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a` tinyint(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE po
INSERT INTO 'po' ('id', 'a') VALUES 
('', '');
----------------------------------------------------

-- CREATE TABLE qwe
CREATE TABLE `qwe` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE qwe
INSERT INTO 'qwe' ('id') VALUES 
('');
----------------------------------------------------

-- CREATE TABLE qwer
CREATE TABLE `qwer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- INSERT DATA TABLE qwer
INSERT INTO 'qwer' ('id') VALUES 
('');
----------------------------------------------------
