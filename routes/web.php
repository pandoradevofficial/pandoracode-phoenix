<?php

/*
|--------------------------------------------------------------------------
| Routes WEB
|--------------------------------------------------------------------------
|
| Di sini anda bisa membangun url / rute anda sendiri
| untuk parameter gunakkan $, misal : 
| 
| Web::url('edit/users/$id','ControllerName@function');
| 
*/

/* Resource buku [TestController] */
Web::resource('buku','TestController');
/* End Resource buku [TestController] */
